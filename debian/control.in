Source: pg-repack
Priority: optional
Section: database
Maintainer: Debian PostgreSQL Maintainers <team+postgresql@tracker.debian.org>
Uploaders:
 Christoph Berg <myon@debian.org>,
 Adrian Vondendriesch <adrian.vondendriesch@credativ.de>,
Build-Depends:
 architecture-is-64-bit <!pkg.postgresql.32-bit>,
 debhelper-compat (= 13),
 postgresql-all <!nocheck>,
 postgresql-server-dev-all,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://github.com/reorg/pg_repack
Vcs-Browser: https://salsa.debian.org/postgresql/pg-repack
Vcs-Git: https://salsa.debian.org/postgresql/pg-repack.git

Package: postgresql-PGVERSION-repack
Architecture: any
Depends:
 ${misc:Depends},
 ${postgresql:Depends},
 ${shlibs:Depends},
Description: reorganize tables in PostgreSQL databases with minimal locks
 pg_repack is a PostgreSQL extension which lets you remove bloat from tables
 and indexes, and optionally restore the physical order of clustered indexes.
 Unlike CLUSTER and VACUUM FULL it works online, without holding an exclusive
 lock on the processed tables during processing. pg_repack is efficient to
 boot, with performance comparable to using CLUSTER directly.
 .
 This package contains the pg_repack program and the server extension for
 PostgreSQL PGVERSION.
